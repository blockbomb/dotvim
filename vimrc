" Begin .vimrc

" initalize plugins using pathogen
call pathogen#infect()
syntax on
filetype plugin indent on

set background=dark
colorscheme gruvbox

" use help 'option' to view the doc(s) on the setting
set autochdir
set softtabstop=2
set shiftwidth=2
set tabstop=2
set expandtab
set hidden
set number
set backspace=indent,eol,start
set incsearch
set hlsearch
set ignorecase smartcase
set pastetoggle=<F2>

"if &listchars ==# 'eol:$'
"  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
"  if &termencoding ==# 'utf-8' || &encoding ==# 'utf-8'
"    let &listchars = "tab:\u21e5 ,trail:\u2423,extends:\u21c9,precedes:\u21c7,nbsp:\u00b7"
"  endif
"endif
"set list

let mapleader = ","
let LustyExplorerSuppressRubyWarning = 1

" Mapping(s)
nnoremap <c-b> :buffers<CR>:buffer<Space>
nnoremap <F5> $beld$-
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Custom Syntax Settings
" For NesC highlighting
augroup filetypedetect
  au! BufRead,BufNewFile *nc set filetype=nc
augroup END

augroup filetypedetect
  au! BufRead,BufNewFile *py set filetype=python
augroup END

" Automatic Command(s)
" Remove trailing whitespace when saving.
autocmd BufWritePre * :%s/\s\+$//e
autocmd FileType python set softtabstop=4
autocmd FileType python set shiftwidth=4
autocmd FileType python set tabstop=4
autocmd FileType python set foldmethod=indent
autocmd FileType pyrex set softtabstop=4
autocmd FileType pyrex set shiftwidth=4
autocmd FileType pyrex set tabstop=4
autocmd FileType pyrex set foldmethod=indent

if &term =~ "xterm"
  " 256 colors
  let &t_Co = 256
  " restore screen after quitting
  let &t_ti = "\<Esc>7\[<Esc>[r\<Esc>[?47h"
  let &t_te = "\<Esc>[?471\<Esc>8"
  if has("terminfo")
    let &t_Sf = "\<Esc>[3%p1%dm"
    let &t_Sb = "\<Esc>[4%p1%dm"
  else
    let &t_Sf = "\<Esc>[3%dm"
    let &t_Sb = "\<Esc>[4%dm"
  endif
endif
" End .vimrc
